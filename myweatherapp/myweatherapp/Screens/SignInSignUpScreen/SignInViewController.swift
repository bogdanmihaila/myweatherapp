//
//  SignInViewController.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func buttonClickedSignIn(_ sender: Any) {
        if let homeViewController = UIStoryboard(name: "HomeScreen", bundle: nil).instantiateViewController(withIdentifier: "Home") as? HomeViewController {
            
            UserDefaults.standard.set(true, forKey: "IsLogged")
            
            if let navigator = navigationController {
                navigator.pushViewController(homeViewController, animated: true)
            }
        }
    }
    @IBAction func buttonClickedNoAccount(_ sender: Any) {
        if let signUpViewController = UIStoryboard(name: "SignUpScreen", bundle: nil).instantiateViewController(withIdentifier: "SignUp") as? SignUpViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(signUpViewController, animated: true)
            }
        }
    }
    @IBAction func buttonClickedRecoverPassword(_ sender: Any) {
        if let recoverViewController = UIStoryboard(name: "RecoverPasswordScreen", bundle: nil).instantiateViewController(withIdentifier: "Recover") as? RecoverPasswordViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(recoverViewController, animated: true)
            }
        }
        
    }
}
