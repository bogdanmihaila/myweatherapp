//
//  SignInSingUpViewController.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import UIKit

class SignInSingUpViewController: UIViewController {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var singUpButton: UIButton!
    @IBOutlet weak var maybeLaterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonClickedSignIn(_ sender: Any) {
        
        if let signInViewController = UIStoryboard(name: "SignInScreen", bundle: nil).instantiateViewController(withIdentifier: "SignIn") as? SignInViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(signInViewController, animated: true)
            }
        }
    }
    
    @IBAction func buttonClickedSignUp(_ sender: Any) {
        if let signUpViewController = UIStoryboard(name: "SignUpScreen", bundle: nil).instantiateViewController(withIdentifier: "SignUp") as? SignUpViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(signUpViewController, animated: true)
            }
        }
    }
    @IBAction func buttonClickedMaybeLater(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "IsLogged")
        if let homeViewController = UIStoryboard(name: "HomeScreen", bundle: nil).instantiateViewController(withIdentifier: "Home") as? HomeViewController {
            present(homeViewController, animated: true, completion: nil)
        }
    }
}
