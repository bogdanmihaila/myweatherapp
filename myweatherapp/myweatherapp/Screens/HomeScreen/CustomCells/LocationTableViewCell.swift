//
//  LocationTableViewCell.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(weatherData: WeatherDataModel) {
        nameLabel.text = weatherData.city
        iconImage.image = UIImage(named: weatherData.weatherIconName)
        tempLabel.text = "Temp:" + String(weatherData.temperature)
        windLabel.text = "Wind:" + String(weatherData.windSpeed) + "km/h"
        coordinatesLabel.text = "Long:" + weatherData.coordLon + " Lat:" + weatherData.coordLat
        precipitationLabel.text = "Prec:" + String(weatherData.precipitation) + "%"
        //For background
        let imageView = UIImageView(frame: CGRect(x: -20, y: 0, width: self.frame.width + 80, height: self.frame.height))
        if weatherData.weatherIconName == "sunny" {
            let image = UIImage(named: "backgroundGrey")
            imageView.image = image
            self.addSubview(imageView)
            self.sendSubviewToBack(imageView)
            self.informationView.addSubview(imageView)
            self.informationView.sendSubviewToBack(imageView)
        } else if weatherData.weatherIconName == "cloudy2" {
            let image = UIImage(named: "backgroundBlack")
            imageView.image = image
            self.addSubview(imageView)
            self.sendSubviewToBack(imageView)
            self.informationView.addSubview(imageView)
            self.informationView.sendSubviewToBack(imageView)
        } else {
            let image = UIImage(named: "backgroundBlue")
            imageView.image = image
            self.addSubview(imageView)
            self.sendSubviewToBack(imageView)
            self.informationView.addSubview(imageView)
            self.informationView.sendSubviewToBack(imageView)
        }
    }
    
}
