//
//  LocationsViewController.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol addNewLocationDelegate: AnyObject {
    func addNewLocation(parameters: [String : String])
}

class LocationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, addNewLocationDelegate {
    
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    let APP_ID = "eaa9a7f9f851c9016eae981179123109"
    
    var locations: [WeatherDataModel] = []
    
    @IBOutlet weak var numberLocations: UILabel!
    @IBOutlet weak var locationsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationsTableView.dataSource = self
        self.locationsTableView.delegate = self
        
        //Register cells
        self.locationsTableView.register(UINib.init(nibName: "LocationTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "locationCell")
        
        self.locationsTableView.rowHeight = 199
        // Do any additional setup after loading the view.
    }
    
    
    func createWeatherModel(json: JSON) {
        let weatherModel = WeatherDataModel()
        let tempResult = json["main"]["temp"].doubleValue
        
        weatherModel.temperature = Int(tempResult - 273.15)
        
        weatherModel.city = json["name"].stringValue
        
        weatherModel.condition = json["weather"][0]["id"].intValue
        
        weatherModel.weatherIconName = weatherModel.updateWeatherIcon(condition: weatherModel.condition)
        
        weatherModel.coordLon = json["coord"]["lon"].stringValue
        weatherModel.coordLat = json["coord"]["lat"].stringValue
        
        weatherModel.windSpeed = json["wind"]["speed"].doubleValue
        weatherModel.precipitation = json["clouds"]["all"].doubleValue
        
        DispatchQueue.main.async(execute: {
            self.locations.append(weatherModel)
            self.numberLocations.text = String(self.locations.count)
            self.locationsTableView.reloadData()
            
        })
        
    }
    
    
    
    func addNewLocation(parameters: [String:String]) {
        APIServices().postMethodWithoutToken(url: WEATHER_URL, parameters: parameters, completionHandler: { (dataJson, error) in
            guard let data = dataJson else {
                if let errorMessage = error?.message {
                    DispatchQueue.main.async(execute: {
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                        if let errorStatusCode = error?.statusCode {
                            alert.title = "Status Code :\(errorStatusCode)"
                        } else {
                            alert.title = "Error"
                        }
                        alert.message = "\(errorMessage)! Please restart the application!"
                        alert.addAction(UIAlertAction(title: "OK. I will restart the app", style: .default))
                        self.present(alert, animated: true)
                    })
                    return
                }
                return
            }
            
            //Success
            let weatherJSON : JSON = JSON(data)
            self.createWeatherModel(json: weatherJSON)
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = locationsTableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as? LocationTableViewCell else { return UITableViewCell() }
        cell.configureCell(weatherData: locations[indexPath.row])
        return cell
    }
    
    @IBAction func buttonNewLocationClicked(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addSeagueId" {
            if let addNewLocationViewController = segue.destination as? AddNewLocationViewController {
                addNewLocationViewController.addDelegate = self
            }
        }
    }
    
    
    
    
}
