//
//  AddNewLocationViewController.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import UIKit

class AddNewLocationViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    static let APP_ID = "eaa9a7f9f851c9016eae981179123109"
    var parameters : [String:String] = ["q" : "", "appid" : APP_ID]
    
    weak var addDelegate: addNewLocationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func buttonBackClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func buttonAddClicked(_ sender: Any) {
        if let location = nameTextField.text {
            parameters["q"] = location
            addDelegate?.addNewLocation(parameters: parameters)
            dismiss(animated: true, completion: nil)
        }
    }
}
