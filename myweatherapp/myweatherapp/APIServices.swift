//
//  APIServices.swift
//  myweatherapp
//
//  Created by MacBook on 07/06/2019.
//  Copyright © 2019 Bogdan Mihaila. All rights reserved.
//

import Foundation


struct ApiError: Error {
    var statusCode: Int?
    var message: String?
}

class APIServices {
    static let domainURL = "http://api.openweathermap.org/data/2.5/weather"
    static let key = "eaa9a7f9f851c9016eae981179123109"
    
    func postMethodWithoutToken(url: String, parameters: [String: String], completionHandler: @escaping(Data?, ApiError?) -> Void) {
        //"?q=Bucharest&appid=eaa9a7f9f851c9016eae981179123109"
        let urlComponents = NSURLComponents(string: url)
        urlComponents?.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        let composedURL = urlComponents?.url
        guard let url = composedURL else {
            let error = ApiError(statusCode: nil, message: "The URL is incorrect!")
            completionHandler(nil, error)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request, completionHandler: { (result, response, error) in
            let httpResponser = response as? HTTPURLResponse
            let code = httpResponser?.statusCode
            guard let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                    let error = ApiError(statusCode: code, message: "Error Call API")
                    completionHandler(nil, error)
                    return
            }
            //Doesn't exist data
            guard let result = result else {
                let error = ApiError(statusCode: nil, message: "Can't load the data! Please restart the application!")
                completionHandler(nil, error)
                return
            }
            //Exist data
            completionHandler(result, nil)
        }).resume()
    }
}
